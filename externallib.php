<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course Categories external lib.
 *
 * @package block_course_categories
 * @copyright 2020 David Watson {@link http://evolutioncode.uk}
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . "/externallib.php");

class block_course_categories_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_category_parameters() {
        return new external_function_parameters(
            array('id' => new external_value(PARAM_INT, 'Category id'))
        );
    }

    /**
     * The function itself
     * @param int $id
     * @return array category contents (sub categories and courses).
     * @throws dml_exception
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     */
    public static function get_category(int $id) {
        // Parameters validation.
        $params = self::validate_parameters(self::get_category_parameters(),
            array('id' => $id));

        // Function \core_course_category::get includes a capability check that current user can see the category.
        $category = \core_course_category::get($params['id'], MUST_EXIST);
        $templateable = new \block_course_categories\output\course_categories();
        $data = $templateable->get_category_data(
            $category, true, has_capability('moodle/category:manage', \context_system::instance())
        );
        $data['showcoursesaslist'] = get_config('block_course_categories', 'showcoursesaslist');
        return $data;
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_category_returns() {

        $courses = new external_multiple_structure(
            new external_single_structure(
                array(
                    'courseid' => new external_value(PARAM_INT, 'id of course'),
                    'name' => new external_value(PARAM_TEXT, 'course name as it should be displayed'),
                    'visible' => new external_value(PARAM_INT, 'visibility of course'),
                    'cardimage' => new external_value(PARAM_TEXT, 'text of url for course card image'),
                )
            )
        );

        return new external_single_structure(
            array(
                'categoryname' => new external_value(PARAM_TEXT, 'name of this category'),
                'visible' => new external_value(PARAM_INT, 'visibility of sub-category'),
                'categoryid' => new external_value(PARAM_INT, 'sub-category id'),
                'course-count' => new external_value(PARAM_INT, 'course count of sub-category'),
                'content-is-loaded' => new external_value(PARAM_INT, 'is content loaded'),
                'content-time' => new external_value(PARAM_INT, 'what time was the content current'),
                'parent-category-id' => new external_value(PARAM_INT, 'parent id of sub-category'),
                'sub-category-count' => new external_value(PARAM_INT, 'sub category count of this category'),
                'depth' => new external_value(PARAM_INT, 'Depth of this category'),
                'next-depth' => new external_value(PARAM_INT, 'Next depth down from this category'),
                'wwwroot' => new external_value(PARAM_TEXT, 'www root URL'),
                'showcoursesaslist' => new external_value(PARAM_INT, 'Show courses as list instead of cards'),
                'courses' => $courses,
                'subcategories' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'categoryname' => new external_value(PARAM_TEXT, 'name of this category'),
                            'visible' => new external_value(PARAM_INT, 'visibility of sub-category'),
                            'categoryid' => new external_value(PARAM_INT, 'sub-category id'),
                            'course-count' => new external_value(PARAM_INT, 'course count of sub-category'),
                            'content-is-loaded' => new external_value(PARAM_INT, 'is content loaded'),
                            'content-time' => new external_value(PARAM_INT, 'what time was the content current'),
                            'parent-category-id' => new external_value(PARAM_INT, 'parent id of sub-category'),
                            'sub-category-count' => new external_value(PARAM_INT, 'sub category count of this category'),
                            'depth' => new external_value(PARAM_INT, 'Depth of this category'),
                            'next-depth' => new external_value(PARAM_INT, 'Next depth down from this category'),
                            'wwwroot' => new external_value(PARAM_TEXT, 'www root URL'),
                            'showcoursesaslist' => new external_value(PARAM_INT, 'Show courses as list instead of cards'),
                            'courses' => $courses,
                            'subcategories' => new external_multiple_structure(
                                // This service only returns one level of sub-categories deep.
                                // Since we at the bottom level of that, we return an empty array of sub-categories.
                                new external_single_structure(array())
                            ),
                            'content-exists' => new external_value(
                                PARAM_INT, 'Does this category have either courses or subcategories'
                            ),
                            'has-courses' => new external_value(PARAM_INT, 'Does this category have courses to display'),
                            'has-sub-categories' => new external_value(
                                PARAM_INT, 'Does this category have subcategories to display'
                            ),
                            'no-content-message' => new external_value(
                                PARAM_TEXT, 'Message to display re missing courses or sub categories'
                            ),
                        )
                    )
                ),
                'content-exists' => new external_value(PARAM_INT, 'Does this category have either courses or subcategories'),
                'has-courses' => new external_value(PARAM_INT, 'Does this category have courses to display'),
                'has-sub-categories' => new external_value(PARAM_INT, 'Does this category have subcategories to display'),
                'no-content-message' => new external_value(PARAM_TEXT, 'Message to display re missing courses or sub categories'),
            )
        );
    }
}