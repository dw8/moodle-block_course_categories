<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_course_categories', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   block_course_categories
 * @copyright 2020 David Watson {@link http://evolutioncode.uk}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['adminview'] = 'Admin view';
$string['allcourses'] = 'Admin user sees all courses';
$string['configadminview'] = 'Whether to display all courses in the Courses block, or only courses that the admin is enrolled in.';
$string['confighideallcourseslink'] = 'Remove the \'All courses\' link under the list of courses. (This setting does not affect the admin view.)';
$string['course_categories:addinstance'] = 'Add a new courses block';
$string['course_categories:myaddinstance'] = 'Add a new courses block to Dashboard';
$string['hideallcourseslink'] = 'Hide \'All courses\' link';
$string['owncourses'] = 'Admin user sees own courses';
$string['pluginname'] = 'My Course Categories';
$string['privacy:metadata'] = 'The Courses block only shows data about courses and does not store any data itself.';
$string['nocontent'] = 'No content available in this category';
$string['nothingtodisplay'] = 'You are not enrolled in any courses and do not have rights to see any categories';
$string['toplevel'] = 'Top level';
$string['showcoursesaslist'] = 'Show courses as list';
$string['showcoursesaslist_desc'] = 'If selected, courses will be shown in a text list (instead of the default rectangular cards with images)';
$string['sortcoursesby'] = 'Sort courses by';
$string['sortcoursesby_desc'] = 'Database field used by this block to sort categories / courses for all users - sortorder refers to the standard sort order used when managing categories and courses';
$string['sortcategoriesby'] = 'Sort categories by';
$string['searchcourses'] = 'Search courses';
$string['toplevelcourses'] = '(Top level courses)';
$string['coursecount'] = 'Number of courses';
$string['subcategorycount'] = 'Number of sub-categories';
$string['loading'] = 'Loading';
$string['manage'] = 'Manage';
$string['notvisible'] = 'Not visible';
$string['togglecategory'] = 'Toggle category';
$string['hidecatswithnocourses'] = 'Hide categories with no courses';
$string['hidecatswithnocourses_desc'] = 'Hide categories I have rights to, if they have no courses, and none of their their sub-categories have courses.
    Switching this to no may make the block respond faster, as it does not have to check all sub-categories for courses.';