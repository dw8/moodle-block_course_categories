// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/* global setTimeout, document, window */
/* eslint space-before-function-paren: 0 */

/**
 * Main Javascript module for block_course_categories
 *
 * @module block_course_categories/main
 * @package block
 * @subpackage  course_categories
 * @copyright 2020 David Watson {@link http://evolutioncode.uk}
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(["jquery", "core/templates", "core/ajax", "core/str"],
    function ($, Templates, ajax) {
        "use strict";

        var SELECTOR = {
            CATEGORY_LOADER: '.category-loader',
            CATEGORY: '.category',
            SPINNER: '#block-spinner',
            ACCORDION: '#accordion',
            ICON: '.icon',
            EYE_SLASH: '.fa-eye-slash',
            BLOCK: '.block_course_categories',
            MANAGE_CATS: '#manage-categories',
            TOP_LEVEL_COURSES: '.top-level-courses',
            COLLAPSIBLE: '.collapsible',
            SUB_CATS_PARENT_ID: '#sub-cats-parent-'
        };
        var ATTR = {
            CONTENT_LOADED: 'data-content-is-loaded',
            CONTENT_TIME: 'data-content-time',
            COLLAPSE_TARGET: 'data-target',
            DATA_PARENT: 'data-parent',
            DATA_CATEGORY_ID: 'data-categoryid'
        };

        var CLASS = {
            TOP_LEVEL_COURSES: 'top-level-courses',
            SHOW: 'show',
            COLLAPSED: 'collapsed',
            LOADING: 'loading'
        };

        // Within 30 seconds we do not reload - show same content.
        var CONTENT_STALE_TIME = Math.floor(Date.now() / 1000) - 30000;

        var timeNow = function () {
            return Math.floor(Date.now() / 1000);
        };

        var addTopLevelCoursesListener = function(categoryid) {
            var topLevelCoursesDiv = $('#top-level-courses-' + categoryid.toString());
            if (topLevelCoursesDiv.length > 0) {
                topLevelCoursesDiv.on('click', function() {
                    var target = $(topLevelCoursesDiv.attr('data-target'));
                    var button = $('#top-level-courses-' + categoryid.toString());
                    var parent = button.attr('data-parent') === "0" ? $(SELECTOR.ACCORDION)
                        : $(SELECTOR.SUB_CATS_PARENT_ID + button.attr('data-parent'));
                    if (button.hasClass(CLASS.COLLAPSED)) {
                        parent.find(SELECTOR.COLLAPSIBLE).slideUp();
                        target.slideDown();
                        parent.find(SELECTOR.CATEGORY_LOADER).addClass(CLASS.COLLAPSED);
                        button.removeClass(CLASS.COLLAPSED);
                    } else {
                        target.slideUp();
                        parent.find(SELECTOR.COLLAPSIBLE).slideUp();
                        parent.find(SELECTOR.CATEGORY_LOADER).addClass(CLASS.COLLAPSED);
                        button.addClass(CLASS.COLLAPSED);
                    }
                });
            }
        };

        var addCategoryListener = function(categoryid) {
            if (!categoryid) {
                return;
            }

            var button = $('#cat-' + categoryid);
            var target = $('#collapse-' + categoryid);
            var parent = button.attr('data-parent') === "0" ? $(SELECTOR.ACCORDION)
                : $(SELECTOR.SUB_CATS_PARENT_ID + button.attr('data-parent'));
            // If a category button is pressed, except a top level courses pseudo category.
            button.on('click', function() {
                // If the category is collapsed and needs the content loaded, expand it and get content from server.
                if (!target.hasClass(CLASS.SHOW)
                    && (!button.attr(ATTR.CONTENT_LOADED) || button.attr(ATTR.CONTENT_TIME) < CONTENT_STALE_TIME)) {
                    button.addClass(CLASS.LOADING);
                    // We are getting content from server and expanding category.
                    if (!target.hasClass(CLASS.TOP_LEVEL_COURSES)) {
                        ajax.call([{
                            methodname: "block_course_categories_get_category",
                            args: {
                                id: button.attr(ATTR.DATA_CATEGORY_ID)
                            }
                        }])[0]
                            .done((templateData) => {
                                Templates.render("block_course_categories/category-sub-content", templateData)
                                    .done(function (html) {
                                        target.html(html);
                                        parent.find(SELECTOR.CATEGORY_LOADER).not(button).addClass(CLASS.COLLAPSED);
                                        parent.find(SELECTOR.COLLAPSIBLE).not(target).slideUp();
                                        target.slideDown();
                                        button.removeClass(CLASS.COLLAPSED);
                                        button.attr(ATTR.CONTENT_LOADED, 1);
                                        button.attr(ATTR.CONTENT_TIME, timeNow());
                                        setTimeout(function() {
                                            button.removeClass(CLASS.LOADING);
                                        }, 500);
                                        templateData.subcategories.forEach((subcat) => {
                                            addCategoryListener(subcat.categoryid, button);
                                        });
                                        addTopLevelCoursesListener(categoryid);
                                    });
                            })
                            .fail((err) => {
                                require(["core/log"], function(log) {
                                    log.debug(err);
                                });
                            });
                    } else {
                        // Top level courses.
                        target.slideDown();
                    }
                } else {
                    // We are just expanding or collapsing item, no need to contact server.
                    if (!button.hasClass(CLASS.COLLAPSED)) {
                        // Item was expanded so we are collapsing it.
                        button.addClass(CLASS.COLLAPSED);
                        parent.find(SELECTOR.COLLAPSIBLE).slideUp();
                        target.slideUp();
                    } else {
                        // We are expanding item and collapsing siblings.
                        parent.find(SELECTOR.CATEGORY_LOADER).not(button).addClass(CLASS.COLLAPSED);
                        parent.find(SELECTOR.COLLAPSIBLE).slideUp();
                        button.removeClass(CLASS.COLLAPSED);
                        target.slideDown();
                    }
                }
            });
        };

        $(document).ready(function() {
            $(SELECTOR.CATEGORY_LOADER).not(SELECTOR.TOP_LEVEL_COURSES).each(function(index, loader) {
                var categoryid = $(loader).attr('data-categoryid');
                addCategoryListener(categoryid, loader);
                addTopLevelCoursesListener(categoryid);
            });

            $(SELECTOR.ACCORDION).slideDown();
            $(SELECTOR.SPINNER).fadeOut();

            if ($(SELECTOR.BLOCK).width() >= 660) {
                // Only show manage cats button if there is room (may not be in remui theme especially).
                $(SELECTOR.MANAGE_CATS).fadeIn();
            }
        });
        return {};
    }
);
