<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course Categories block output.
 *
 * @package block_course_categories
 * @copyright 2020 David Watson {@link http://evolutioncode.uk}
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace block_course_categories\output;

defined('MOODLE_INTERNAL') || die();

/**
 * Prepares data for user report (section at top with overall stats then charts if required).
 *
 * @package block_course_categories
 * @copyright 2020 David Watson {@link http://evolutioncode.uk}
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class course_categories implements \renderable, \templatable {

    private $basecolours;

    /**
     * Export the data for the mustache template.
     */
    public function export_for_template(\renderer_base $output) {
        global $CFG, $SESSION;
        $hassitewidemangerights = has_capability('moodle/category:manage', \context_system::instance());
        $context = [
            'showcoursesaslist' => get_config('block_course_categories', 'showcoursesaslist'),
            'hassitewidemanagerights' => $hassitewidemangerights,
            'wwwroot' => $CFG->wwwroot
        ];
        // On initial render, we want to show the user's top level categories, collapsed.
        // We will then populate them on click with webservices.
        if ($hassitewidemangerights) {
            // USE CASE 1 - Site admin or rights to manage all categories site wide.
            $toplevelcategories = \core_course_category::top()->get_children(
                array('sort' => array(get_config('block_course_categories', 'sortcategoriesby') => 1))
            );
        } else {
            self::initialise_session_vars();

            // This is USE CASE 2 - categories where I have a role assigned.
            $this->set_my_assigned_category_ids();

            // This is USE CASE 3 - categories where I have course enrolments.
            $this->set_my_enrolled_category_ids();

            // Use whatever we now have in session from the above.
            $toplevelcategories = $this->get_category_objects($SESSION->block_course_cats_top_cat_ids);
        }
        $count = count($toplevelcategories);
        if ($count > 0 ) {
            // Sort cats in order selected by Site Admin. See compare_sort_cats_by_name() and compare_sort_cats_by_sortorder().
            usort($toplevelcategories, 'self::compare_sort_cats_by_' . get_config('block_course_categories', 'sortcategoriesby'));

            // Preload child courses and categories.
            // We only go 1 level deep and leave the rest to web services.
            foreach ($toplevelcategories as $toplevelcategory) {
                // We include a true argument here to load one level down.
                $data = $this->get_category_data($toplevelcategory, true, $hassitewidemangerights);
                if ($data['has-courses'] || $data['has-sub-categories']) {
                    // If the category has no courses or sub-categories visible to us, we skip displaying it.
                    $context['toplevelcategories'][] = $data;
                }

            }
        }
        // If we only have one top level category, expand it.
        $context['top-level-expanded'] = $count == 1;

        return $context;
    }

    /**
     * Initialise the vars we use to store ids in session.
     */
    private static function initialise_session_vars() {
        global $SESSION;
        $SESSION->block_course_cats_top_cat_ids = [];
        $SESSION->block_course_cats_cat_paths = [];
    }

    /**
     * Get the categories where I have a role assigned.
     * @throws \dml_exception
     */
    private function set_my_assigned_category_ids() {
        global $DB, $USER, $SESSION;
        // USE CASE 2 - User with role assigned to categories.
        // We extract the whole path from the database as the SQL is not the same for all dbs, then consolidate in PHP.
        $sql = "SELECT DISTINCT cc.id, cc.path
            FROM {role_assignments} ra
            JOIN {user} u ON u.id = ra.userid
            JOIN {context} ctx ON ra.contextid = ctx.id
            JOIN {role} r on r.id = ra.roleid
            JOIN {course_categories} cc on cc.id = ctx.instanceid
            WHERE ctx.contextlevel = :contextlevel
            AND ra.userid = :userid";

        $mycats = $DB->get_records_sql(
            $sql,
            ['contextlevel' => CONTEXT_COURSECAT, 'userid' => $USER->id]
        );
        if (!empty($mycats)) {
            // Convert to set based and store top level and all levels in session.
            foreach ($mycats as $cat) {
                $explodedpath = explode('/', $cat->path);
                // Cat path starts with / so we want item 1 e.g. /6/9 => 6.
                $SESSION->block_course_cats_top_cat_ids[$explodedpath[1]] = $explodedpath[1];
                // We store all category paths in session to know which paths we have rights to later.
                $SESSION->block_course_cats_cat_paths[$cat->path] = $cat->path;
            }
        }
    }

    /**
     * Get the top level category (or categories) for all my current enrolments.
     * @throws \dml_exception
     */
    private function set_my_enrolled_category_ids() {
        global $DB, $USER, $SESSION;

        // USE CASE 3 - Use user course enrolments to establish category list.
        $sql = "SELECT DISTINCT cc.path
            FROM {user_enrolments} ue
            JOIN {enrol} e on e.id = ue.enrolid
            JOIN {course} c ON c.id = e.courseid
            JOIN {course_categories} cc on cc.id = c.category
            WHERE ue.userid = :userid
            AND e.status = 0 AND ue.status = 0
            AND (ue.timestart = 0 OR ue.timestart < :timenow)
            AND (ue.timeend = 0 OR ue.timeend > :timenow2)
            AND (e.enrolstartdate = 0 OR e.enrolstartdate > :timenow3)
            AND (e.enrolenddate = 0 OR e.enrolenddate < :timenow4)";

        $mycatpaths = $DB->get_records_sql($sql,
            ['timenow' => time(), 'timenow2' => time(), 'timenow3' => time(), 'timenow4' => time(), 'userid' => $USER->id]
        );
        if (!empty($mycatpaths)) {
            // Create a set of cat ids based on first item in cat path only - we only want top level cats.

            foreach ($mycatpaths as $catpath) {
                $explodedpath = explode('/', $catpath->path);
                // Cat path starts with / so we want item 1 e.g. /6/9 => 6.
                $SESSION->block_course_cats_top_cat_ids[$explodedpath[1]] = $explodedpath[1];
                // We store all category paths in session to know which paths we have rights to later.
                $SESSION->block_course_cats_cat_paths[$catpath->path] = $catpath->path;
            }
        }
    }

    /**
     * Is the given category id in my category paths (for assigned categories or enrolled).
     * @param string $searchpath the category path we are checking.
     */
    private function in_my_cat_paths(string $searchpath): bool {
        global $SESSION;
        if (!isset($SESSION->block_course_cats_cat_paths) || empty($SESSION->block_course_cats_cat_paths)) {
            return false;
        }
        foreach ($SESSION->block_course_cats_cat_paths as $allowedpath) {
            // If the path we are checking is a sub path of one we are assigned to, we include it.
            // E.g. if the path we are checking is /1/6/9/10 and we are assigned to /1/6 then return true.
            // Also needs to work other way around e.g. if we are checking /1/6 and allowed /1/6/9/10 then true.
            if (strpos($allowedpath, $searchpath) !== false || strpos($searchpath, $allowedpath) !== false) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get category objects from a series of IDs, if user can see the categories.
     * @param array $ids
     * @return array
     */
    private function get_category_objects(array $ids): array {
        if (empty($ids)) {
            return [];
        }

        $catobjects = \core_course_category::get_many($ids);

        // Check that categories are visible to this user (may have been hidden at category level or be outside our paths).
        $visiblecats = [];
        foreach ($catobjects as $cat) {
            if ($cat->is_uservisible() && $this->in_my_cat_paths($cat->path)) {
                $visiblecats[] = $cat;
            }
        }

        return $visiblecats;
    }

    /**
     * Comparison function for usort above to sort categories
     * @see get_my_top_level_categories()
     * @param $a
     * @param $b
     * @return bool
     */
    public static function compare_sort_cats_by_name($a, $b) {
        return $b->name < $a->name;
    }

    /**
     * Comparison function for usort above to sort categories
     * @see get_my_top_level_categories()
     * @param $a
     * @param $b
     * @return bool
     */
    public static function compare_sort_cats_by_sortorder($a, $b) {
        return (int)$a->sortorder - (int)$b->sortorder;
    }


    /**
     * Get the data for one category.  This is called on page render for top level categories.
     * It is also called by web services AJAX for each unloaded category clicked after page load.
     * @param \core_course_category $category
     * @param bool $includedirectchildren should we go down one more level recursively
     * @param bool $hassitesidemanagerrights does the user have site wide manager rights?
     * @return array $category data
     * @throws \moodle_exception
     */
    public function get_category_data(\core_course_category $category,
                                      bool $includedirectchildren = false, bool $hassitesidemanagerrights = false) {
        global $CFG;
        $coursesaslist = get_config('block_course_categories', 'showcoursesaslist');
        $cat = array(
            'categoryname' => $category->get_formatted_name(),
            'visible' => $category->visible,
            'categoryid' => $category->id,
            'course-count' => 0, // We may overwrite this below.
            'content-is-loaded' => 0,
            'content-time' => 0,
            'parent-category-id' => $category->parent,
            'sub-category-count' => $category->get_children_count(),
            'depth' => $category->depth,
            'next-depth' => $category->depth + 1,
            'wwwroot' => $CFG->wwwroot, // Include this for use when rendered from JS.
            'showcoursesaslist' => $coursesaslist ? 1 : 0, // Need this here for JS calls.
            'courses' => [], // We may overwrite this below.
            'subcategories' => [] // We may overwrite this below.
        );
        $cat['has-sub-categories'] = $cat['sub-category-count'] ? 1 : 0;

        // Even if not displaying children, we need to know the course count, to decide whether to skip this cat.
        $courses = $category->get_courses(
            ['sort' => array(get_config('block_course_categories', 'sortcoursesby') => 1)]
        );

        if (!empty($courses)) {
            foreach ($courses as $course) {
                if ($course->can_access()) {
                    $cat['course-count']++;
                    // If we are showing children, we also need the detail of each course for this category.
                    if ($includedirectchildren) {
                        $cat['courses'][] = array(
                            'courseid' => $course->id,
                            'name' => format_string(
                                $course->fullname, true, array('context' => \context_course::instance($course->id))
                            ),
                            'visible' => $course->visible,
                            'cardimage' => $coursesaslist ? '' : $this->get_course_image($course)
                        );
                    }
                }
            }
        }
        $cat['has-courses'] = $cat['course-count'] ? 1 : 0;

        if ($cat['sub-category-count']) {
            $subcategories = $category->get_children(
                ['sort' => array(get_config('block_course_categories', 'sortcategoriesby') => 1)]
            );
            $countvisiblecategories = 0;
            foreach ($subcategories as $subcategory) {
                // If we are enrolled in courses in the sub-category or can see them.
                if (($hassitesidemanagerrights || $this->in_my_cat_paths($subcategory->path))
                    && $this->is_interesting($subcategory)) {
                    $countvisiblecategories++;
                    if ($includedirectchildren) {
                        $data = $this->get_category_data($subcategory, false, $hassitesidemanagerrights);
                        $cat['subcategories'][] = $data;
                        $cat['content-is-loaded'] = 1;
                        $cat['content-time'] = time();
                    }
                }
            }
            $cat['sub-category-count'] = $countvisiblecategories;
            $cat['has-sub-categories'] = $countvisiblecategories > 0 ? 1 : 0;
        }

        $cat['no-content-message'] = empty($cat['courses']) && empty($cat['subcategories'])
            ? get_string('nocontent', 'block_course_categories') : '';
        $cat['content-is-loaded'] = !empty($cat['subcategories']) || !empty($cat['courses']) ? 1 : 0;
        $cat['content-time'] = !$cat['content-is-loaded'] ? 0 : time();
        $cat['content-exists'] = !empty($cat['subcategories']) || !empty($cat['courses']) ? 1 : 0;
        return $cat;
    }

    /**
     * Is this category interesting to the user?  I.e. does it have a course I can see in it (even at a lower level).
     * @param \core_course_category $category the category of interest.
     * @return bool
     * @throws \coding_exception
     * @throws \dml_exception
     * @throws \moodle_exception
     */
    public function is_interesting(\core_course_category $category) {
        global $DB, $USER, $SESSION;
        if (!get_config('block_course_categories', 'hidecatswithnocourses')) {
            // Admin has decided not to hide categories with no courses, so all categories are interesting.
            return true;
        }
        $categorycontext = $category->get_context();
        if (has_capability('moodle/category:manage', $categorycontext)
            || has_capability('moodle/course:viewhiddencourses', $categorycontext)) {
            // If I can manage category or see hidden courses, one course in it makes category interesting, even if not enrolled.
            // So we need to check this category's sub-categories recursively for courses visible to this user.
            // This could be a little slow on first run as it's recursive, but results of $category->get_courses() are cached.
            $courses = $category->get_courses(array('recursive' => 1, 'limit' => 1));
            return !empty($courses);
        }

        // If we are still here, now need to check if any of my enrolments are in this category or sub categories.
        // First get the category ids for the paths of all courses I am enrolled in.
        if (!isset($SESSION->block_coursecats_myenrolledcats) || !isset($SESSION->block_coursecats_myenrolledcourseids)
            || $SESSION->block_coursecats_myenrolledcatstime < strtotime('-3 minutes')) {
            // We need course id in query too as add all checked courses to $SESSION->block_coursecats_myenrolledcourseids below.
            $sql = "SELECT c.id, cc.path
                FROM {user_enrolments} ue
                JOIN {enrol} e on e.id = ue.enrolid
                JOIN {course} c ON c.id = e.courseid
                JOIN {course_categories} cc on cc.id = c.category
                WHERE ue.userid = :userid
                AND e.status = 0
                AND (ue.timeend = 0 OR ue.timeend > :timenow)";

            $records = $DB->get_records_sql($sql, ['timenow' => time(), 'userid' => $USER->id]);
            $cats = [];
            if (!empty($records)) {
                // Convert to set of all items in all paths.
                foreach ($records as $record) {
                    $catids = explode('/', $record->path);
                    if (!empty($catids)) {
                        foreach ($catids as $id) {
                            if ($id) {
                                $cat = \core_course_category::get($id, false);
                                // Cat will be null if the category is not visible to this user.
                                if ($cat) {
                                    $cats[$id] = $id;
                                }
                            }
                        }
                    }
                }
            }
            // Store values so we don't have to repeat for a while.
            $SESSION->block_coursecats_myenrolledcats = $cats;
            $SESSION->block_coursecats_myenrolledcourseids = array_keys($records);
            $SESSION->block_coursecats_myenrolledcatstime = time();
        }

        // Next check if this category is one of those I have an enrolment for.
        if (empty($SESSION->block_coursecats_myenrolledcats)
            || !in_array($category->id, $SESSION->block_coursecats_myenrolledcats)) {
            return false;
        }

        // Now get category courses to check what is actually visible to me (even though enrolled).
        // Get courses recursively could be slow, but result will be cached for future use by other users.
        // First try it non recursively for speed, then recursively if needed.
        if ($this->category_has_enrolled_course($category, false)) {
            return true;
        }

        if ($this->category_has_enrolled_course($category, true)) {
            return true;
        }

        // This category is not interesting to me as nothing for me to see in it.
        return false;
    }

    private function category_has_enrolled_course($category, bool $recursive = false) {
        global $SESSION;
        $params = $recursive ? ['recursive' => 1] : null;
        $categorycourses = $category->get_courses($params);
        if (empty($categorycourses)) {
            return false;
        }
        foreach ($categorycourses as $course) {
            if (in_array($course->id, $SESSION->block_coursecats_myenrolledcourseids)) {
                if ($course->visible || has_capability('moodle/course:viewhiddencourses', \context_course::instance($course->id))) {
                    $coursecat = \core_course_category::get($course->category, false);
                    if ($coursecat) {
                        // I am enrolled and the course is visible to me, so the category is interesting to me.
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Get the course image if added to course.
     * @param \core_course_list_element $courselistelement
     * @return string url of course image
     * @throws \moodle_exception
     */
    public function get_course_image(\core_course_list_element $courselistelement) {
        foreach ($courselistelement->get_course_overviewfiles() as $file) {
            if ($file->is_valid_image()) {
                $pathcomponents = [
                    '/pluginfile.php',
                    $file->get_contextid(),
                    $file->get_component(),
                    $file->get_filearea() . $file->get_filepath() . $file->get_filename()
                ];
                $path = implode('/', $pathcomponents);
                return (new \moodle_url($path))->out();
            }
        }

        if (!$this->basecolours) {
            $colournumbers = range(1, 10);
            foreach ($colournumbers as $number) {
                $this->basecolours[] = get_config('core_admin', 'coursecolor' . $number);
            }
        }

        $colour = $this->basecolours[$courselistelement->id % 10];
        $pattern = new \core_geopattern();
        $pattern->setColor($colour);
        $pattern->patternbyid($courselistelement->id);
        return $pattern->datauri();
    }
}

