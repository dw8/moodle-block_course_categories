<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course list block settings
 *
 * @package    block_course_categories
 * @copyright  2020 David Watson {@link http://evolutioncode.uk}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    $name = 'block_course_categories/showcoursesaslist';
    $title = get_string('showcoursesaslist', 'block_course_categories');
    $description = get_string('showcoursesaslist_desc', 'block_course_categories');
    $default = 0;
    $settings->add(new admin_setting_configcheckbox($name, $title, $description, $default));

    $sortorders = [];
    foreach (['shortname', 'fullname', 'sortorder'] as $order) {
        $sortorders[$order] = $order;
    }

    $setting = new admin_setting_configselect(
        'block_course_categories/sortcoursesby',
        get_string('sortcoursesby', 'block_course_categories'),
        get_string('sortcoursesby_desc', 'block_course_categories'),
        'shortname',
        $sortorders
    );
    $settings->add($setting);

    $sortorders = [];
    foreach (['name', 'sortorder'] as $order) {
        $sortorders[$order] = $order;
    }

    $setting = new admin_setting_configselect(
        'block_course_categories/sortcategoriesby',
        get_string('sortcategoriesby', 'block_course_categories'),
        get_string('sortcoursesby_desc', 'block_course_categories'),
        'sortorder',
        $sortorders
    );
    $settings->add($setting);

    $name = 'block_course_categories/hidecatswithnocourses';
    $title = get_string('hidecatswithnocourses', 'block_course_categories');
    $description = get_string('hidecatswithnocourses_desc', 'block_course_categories');
    $default = 1;
    $settings->add(new admin_setting_configcheckbox($name, $title, $description, $default));
}


