# My Course Categories

Author: David Watson
Date: February 2020

A block for Moodle, intended to be displayed on the user dashboard.
 
### Usage

The block shows course categories and when each category is clicked, it expands to show sub categories and courses.
 
It is intended to be used by three categories of staff (the three use cases).  For each use case the list of categories show is generated differently. 

* administrators - they will see all course categories site wide (this class also includes any user who has the right to manage categories system wide, e.g. a site wide manager).
* category managers - users assigned the role of manager at category level (but not system wide) will be shown each category for which they have that role assigned
* other users - users who are enrolled into courses, but who do not have category roles, will see a list of the categories to which their courses belong.

### Plugin settings

A site administrator can set (under Site Admin > Plugins > Blocks > My Course Categories) the following:

* whether courses are displayed as a deck of 'cards' with course images, or a simple textual bullet list

* the sort orders for categories and (separately) courses (i.e. should they be sorted by name or the sort order set under manage categories) 